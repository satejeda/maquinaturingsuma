function soloNumeros(e) {
	// capturamos la tecla pulsada
	var teclaPulsada = window.event ? window.event.keyCode : e.which;
	// capturamos el contenido del input
	var valor = document.getElementById("inputNumero").value;
	// devolvemos true o false dependiendo de si es numerico o no
	return /\d/.test(String.fromCharCode(teclaPulsada));
}

var velocidad=1, direccion=velocidad,iniciar=false,x=22,y=66;
var intervalo;
var valor,p=0;
var automata = [ [ "0,0", "0,1" ], [ "1,2", "1,1" ] ]; 
window.addEventListener('load',init);
function init(){
    var canvas=document.getElementById("micanvas");
    var ctx = canvas.getContext("2d");

    document.getElementById('cargar').addEventListener('click',function(){ 
        valor=convertir(document.getElementById('txt').value+'+'+document.getElementById('txt2').value);
        if(valor!=''){
            x=22;
            window.clearInterval(intervalo);
            canvas.width=canvas.width;
            ctx.strokeRect(63,40,20,35);
            cinta(ctx,x,y);
            iniciar=false;
        }
    });    
    
        document.getElementById('boton').addEventListener('click',function(){ 
            window.clearInterval(intervalo);
            if(!iniciar){
                intervalo=window.setInterval(function(){
                    moveAndDraw(canvas,ctx);
                    if(x==66){
                        window.clearInterval(intervalo);
                        iniciar=true;
                        velocidad=22;
                        suma(valor,canvas,ctx);
                        //velocidad=1;
                    }
                },32);
            }
        });    
}

function draw(canvas,ctx,x,y){
    
        canvas.width=canvas.width;
        cinta(ctx,x,y);
        ctx.strokeRect(63,40,20,35);
    
}



function moveAndDraw(canvas,ctx){
    
    if(x<66 && iniciar==false){
        direccion=velocidad;
        x += direccion;
        draw(canvas,ctx,x,y);
    }else{
            direccion=-velocidad;
            x += direccion;
            draw(canvas,ctx,x,y);
    }
    
}

function cinta(ctx,x,y){
    p=0;
    for(var i=0;i< valor.length; i++){
        ctx.font = "30px Times New Roman";
        ctx.strokeText(valor[i],x+p,y);
        p=p+22;
    }

}

function convertir(dato){
    var v=dato.split('+');
    var inf='00';
    
    for(var i=0;i<v[0];i++){
        inf=inf+'1';
    }
    inf=inf+'0';
    for(var j=0;j<v[1];j++){
        inf=inf+'1';
    }
    inf=inf+'0';
    return inf;
}


function suma(dato,canvas,ctx){
        var canvas=document.getElementById("micanvas");
        var ctx = canvas.getContext("2d");
        var v=dato.split(''); 
        var est=0,i=0; 
            window.clearInterval(intervalo);
            intervalo=window.setInterval(function(){
                if(i<v.length){
                    if(est!=2){
                        est = getState(est,v[i]);

                        if (v[i] == 0 && est == 0) {
                            v[i] = 0;
                        } else if (v[i] == 1 && est == 0) {
                            v[i] = 0;                       
                            est = 1;
                        }else if (v[i] == 1 && est == 1) {
                            v[i] = 1;                        
                        } else if (v[i] == 0 && est == 1) {
                            v[i] = 1; 
                            est = 2;                    
                        }
                    }
                    draw(canvas,ctx,x,y);
                    valor=recorrer(v);
                    moveAndDraw(canvas,ctx);
                    
                    i++;
                }else{
                    window.clearInterval(intervalo);
                    iniciar=false;
                    velocidad=1;
                    intervalo=window.setInterval(function(){
                        moveAndDraw(canvas,ctx);
                    if(x==66){
                        window.clearInterval(intervalo);
                    }
                },32);
                    
                }
            },1000);
}

function recorrer(vect){
    var info='';
    for(var i=0;i<vect.length;i++){
        info=info+vect[i];
    }
    return info;
}


function getState(state, leido) {
	var v = automata[state][leido].split(',');
        
	var a = v[0];
	return a;
}